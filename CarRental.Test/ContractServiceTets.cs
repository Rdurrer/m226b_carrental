﻿using CarRental.Classes;
using CarRental.Services;
using System;
using FluentAssertions;
using System.Collections.Generic;
using System.Text;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CarRental.Test
{
    public class ContractServiceTets
    {
        private readonly ContractService contractService;

        [Fact]
        public void Pay_InCash()
        {
            // Arrange
            Car car = new Car();
            Client client = new Client();
            RentalContract contract = new RentalContract
            {
                Vehicle = car,
                Client = client,
                SignDate = DateTime.Now,
                StartTime = DateTime.Now,
                EndTime = DateTime.Now,
                PickupPoint = "Zug",
                DropOffPoint = "Zug"
            };

            // Result
            double result = contractService.Pay(Enums.PaymentMethod.Cash, contract, 200.20, 100.10);

            //
            result.Should().Be(100.10);
        }
    }
}
