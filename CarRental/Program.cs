﻿using System;
using CarRental.Services;
using CarRental.Services.Interfaces;
using CarRental.Handlers;
using Figgle;
using EventHandler = CarRental.Handlers.EventHandler;


namespace CarRental
{
    class Program
    {
        static void Main(string[] args)
        {
            EventHandler eventHandler = new EventHandler();
            eventHandler.WriteWelcome();
        }
    }
}
