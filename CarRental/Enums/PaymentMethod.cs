﻿namespace CarRental.Enums
{
    public enum PaymentMethod
    {
        Cash,
        Card
    }
}