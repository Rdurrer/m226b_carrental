﻿namespace CarRental.Enums
{
    public enum Brand
    {
        Mercedes,
        Toyota,
        Tesla,
        Honda,
        Hyundai,
        Renault,
        Porsche,
        Volkswagen,
        Audi,
        Dodge,
        Ford,
        Chrysler
    }
}