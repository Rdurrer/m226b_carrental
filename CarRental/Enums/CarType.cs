﻿namespace CarRental.Enums
{
    public enum CarType
    {
        Economy,
        EstateCar,
        Luxury,
        Sports,
        Super,
        Van,
        Electric
    }
}