﻿namespace CarRental.Enums
{
    public enum Tyre
    {
        Snow,
        Dry,
        Slick
    }
}