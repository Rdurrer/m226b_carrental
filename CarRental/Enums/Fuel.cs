﻿namespace CarRental.Enums
{
    public enum Fuel
    {
        Diesel,
        Bleifrei,
        Kerosin
    }
}