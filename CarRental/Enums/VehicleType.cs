﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Enums
{
    public enum VehicleType
    {
        car,
        truck,
        motorcycle
    }
}

