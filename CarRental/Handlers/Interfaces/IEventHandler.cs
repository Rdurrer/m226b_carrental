﻿using System;
using System.Collections.Generic;
using CarRental.Classes;

namespace CarRental.Handlers.Interfaces
{
    public interface IEventHandler
    {
        /// <summary>
        /// Writes welcome and menu for start of program
        /// </summary>
        /// <param name="isinvalid"></param>
        public void WriteWelcome(bool isinvalid = false);

        /// <summary>
        /// Asks user if he or she is already a customer
        /// </summary>
        /// <param name="isinvalid"></param>
        public void AskIfAlreadyCustomer(bool isinvalid = false);

        /// <summary>
        /// Asks the customer which account to use
        /// </summary>
        public Client AskWhichCustomer(bool isValid = false);

        /// <summary>
        /// Asks the customer which vehicle he wants to rent
        /// </summary>
        /// <returns></returns>
        public Vehicle AskForVehicle(DateTime startDate, DateTime endDate, bool isinvalid = false);

        /// <summary>
        /// Creates the contract
        /// </summary>
        public void DoThePaperwork(Client client, bool isinvalid = false);

        /// <summary>
        /// Asks user which statistics he or she wants to see
        /// </summary>
        public void SeeStatistics(bool isinvalid = false);
        
        /// <summary>
        /// Lists all currently available employees for the client to choose from
        /// </summary>
        /// <returns>A list of available employees</returns>
        public List<SalesEmployee> GetAvailableEmployees();

        /// <summary>
        /// Assigns an employee to a client
        /// </summary>
        /// <param name="salesEmployee">The employee to be assigned</param>
        /// <param name="client">To which client the employee should be assigned to</param>
        public void AssignEmployee(SalesEmployee salesEmployee, Client client);

        /// <summary>
        /// Unassigns an employee
        /// </summary>
        /// <param name="salesEmployee">The employee to be unassigned</param>
        public void UnassignEmployee(SalesEmployee salesEmployee);
    }
}