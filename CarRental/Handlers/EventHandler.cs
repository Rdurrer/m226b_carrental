﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using CarRental.Classes;
using CarRental.Enums;
using CarRental.Handlers.Interfaces;
using CarRental.Services;
using Figgle;

namespace CarRental.Handlers
{
    public class EventHandler : IEventHandler
    {
        public List<SalesEmployee> GetAvailableEmployees()
        {
            List<SalesEmployee> salesEmployees = new List<SalesEmployee>();
            using (Context Db = new Context())
            {
                salesEmployees = Db.SalesEmployees.Where(s => s.IsAvailable).ToList();
            }
            return salesEmployees;
        }

        public void AssignEmployee(SalesEmployee salesEmployee, Client client)
        {
            salesEmployee.IsAvailable = false;
        }

        public void UnassignEmployee(SalesEmployee salesEmployee)
        {
            salesEmployee.IsAvailable = true;
        }
        public void WriteWelcome(bool isinvalid = false)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(
                FiggleFonts.Slant.Render("Car-Rental")
            );
            Console.WriteLine("Welcome to the CarRental!");
            Console.WriteLine("Please press the number refering to the action you'd like to take.");
            Console.WriteLine("1) Rent a Vehicle");
            Console.WriteLine("2) Look at statistics");

            if (isinvalid)
            {
                Console.WriteLine("Please enter a valid menu option");
            }
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D1:
                    AskIfAlreadyCustomer();
                    break;
                case ConsoleKey.D2:
                    SeeStatistics();
                    break;
                default:
                    WriteWelcome(true);
                    break;
            }
        }

        public void AskIfAlreadyCustomer(bool isinvalid = false)
        {
            Client client = new Client();
            Console.Clear();
            Console.WriteLine(
                FiggleFonts.Slant.Render("Car-Rental")
            );
            Console.WriteLine("Are you already a customer?");
            Console.WriteLine("1) Yes");
            Console.WriteLine("2) No");
            if (isinvalid)
            {
                Console.WriteLine("Please enter a valid menu option");
            }
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D1:
                    // Already customer
                    client = AskWhichCustomer();
                    DoThePaperwork(client);
                    break;
                case ConsoleKey.D2:
                    // Not yet customer
                    ClientService clientService = new ClientService();
                    client = clientService.CreateClient();
                    DoThePaperwork(client);
                    break;
                default:
                    AskIfAlreadyCustomer(true);
                    break;
            }
        }


        public Client AskWhichCustomer(bool isinvalid = false)
        {

            ClientService clientservice = new ClientService();
            Client client = new Client();
            Console.Clear();
            Console.WriteLine(
                FiggleFonts.Slant.Render("Car-Rental")
            );
            if (isinvalid)
            {
                Console.WriteLine("No user with that username found!");
            }
            Console.WriteLine("Please enter your username:");
            string enteredUsername = Console.ReadLine();
            Client tempClient = clientservice.GetClient(enteredUsername);
            if (tempClient == null)
            {
                AskWhichCustomer(true);
                return null;
            }
            else
            {
                return tempClient;
            }
            
        }

        public void AskForEmployee(Client client)
        {
            List<SalesEmployee> availablesalesEmployees = GetAvailableEmployees();
            Console.Clear();
            Console.WriteLine(
                FiggleFonts.Slant.Render("Car-Rental")
            );
            Console.WriteLine("Please type the id of the employee you'd like to work with:");
            foreach (SalesEmployee employee in availablesalesEmployees)
            {
                Console.WriteLine(employee.Id + " " + employee.FirstName + " " + employee.LastName);
            }
            Console.WriteLine("--------");
            Console.WriteLine("Id:");
            int chosenId = Convert.ToInt32(Console.ReadLine());

            ClientService clientService = new ClientService();
            


        }

        public void ReturnVehicle()
        {
            //TODO: change and add property vehicle name to vehicle class and seed it
            ClientService clientService = new ClientService();
            Client client = AskWhichCustomer();
            List<RentalContract> activeContracts = clientService.GetActiveContracts(client);
            Console.WriteLine("Please enter the Id of the contract youd like to terminate");
            foreach (RentalContract contract in activeContracts)
            {
                Console.WriteLine("Id = " + contract.RentalContractId + "   Vehicle = " + contract.Vehicle.Brand + "   " + contract.Vehicle.PricePerDay);
            }
            // continue here
        }

        public void DoThePaperwork(Client client, bool isinvalid = false)
        {
            VehicleService vehicleService = new VehicleService();
            ContractService contractService = new ContractService();
            ClientService clientService = new ClientService();
            Console.Clear();
            AskForEmployee(client);
            DateTime startTime = AskForStartDate();
            DateTime endTime = AskForEndDate();
            Vehicle vehicle = AskForVehicle(startTime, endTime);
            DateTime signDate = DateTime.Now;
            RentalContract contract = new RentalContract
            {
                Client = client,
                SalesEmployee = client.AssignedEmployee,
                Vehicle = vehicle,
                SignDate = signDate,
                StartTime = startTime,
                EndTime = endTime,
                IsActive = true
            };
            vehicleService.LockVehicle(vehicle);
            contractService.SaveContract(contract);
            clientService.UnassignEmployee(client, client.AssignedEmployee.Id);
        }

        public DateTime AskForStartDate(bool isValid = false)
        {
            Console.Clear();
            Console.WriteLine(
                FiggleFonts.Slant.Render("Car-Rental")
            );
            Console.WriteLine("Please enter the start date of the rental");
            DateTime startDate = DateTime.Now;
            do
            {
                try
                {
                    startDate = DateTime.Parse(Console.ReadLine());
                    isValid = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Please enter the date in the following format! dd/mm/yyyy");
                }
            } while (!isValid);
            isValid = false;
            return startDate;
        }

        public DateTime AskForEndDate(bool isValid = false)
        {
            Console.Clear();
            Console.WriteLine(
                FiggleFonts.Slant.Render("Car-Rental")
            );
            Console.WriteLine("Please enter the end date of the rental");
            DateTime endDate = DateTime.Now;
            do
            {
                try
                {
                    endDate = DateTime.Parse(Console.ReadLine());
                    isValid = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Please enter the date in the following format! dd/mm/yyyy");
                }
            } while (!isValid);
            isValid = false;
            return endDate;
        }

        public Vehicle AskForVehicle(DateTime startDate, DateTime endDate, bool isinvalid = false)
        {
            VehicleService vehicleService = new VehicleService();
            bool isValid = false;

            Console.Clear();
            Console.WriteLine(
                FiggleFonts.Slant.Render("Car-Rental")
            );

            Console.WriteLine("What kind of vehicle would you like to rent?");
            Console.WriteLine("1) Car");
            Console.WriteLine("2) Truck");
            Console.WriteLine("3) Motorcycle");
            if (isinvalid)
            {
                Console.WriteLine("Please enter a valid menu option");
            }
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D1:
                    List<int> idList = new List<int>();
                    List<Car> availableCars = vehicleService.GetAvailableCars(startDate, endDate);
                    Console.Clear();
                    Console.WriteLine(
                        FiggleFonts.Slant.Render("Car-Rental")
                    );
                    Console.WriteLine("Please type the id of the Car you'd like to rent:");
                    foreach (Car car in availableCars)
                    {
                        idList.Add(car.Id);
                        Console.WriteLine("Id = " + car.Id + "   Brand = " + car.Brand + "   Type = " + car.Type + "   Lenght = " + car.Length + "   Width = " + car.Width + "   Price per day = " + car.PricePerDay);
                    }
                    Console.WriteLine("--------");
                    Console.WriteLine("Id:");
                    int chosenCarId = Convert.ToInt32(Console.ReadLine());

                    using (Context db = new Context())
                    {
                        try
                        {
                            Car chosenCar = db.Cars.FirstOrDefault(c => c.Id == chosenCarId);
                            return chosenCar;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"An Error happened try again please:{e}");
                            throw;
                        }

                    }

                    break;

                case ConsoleKey.D2:
                    List<Truck> availableTrucks = vehicleService.GetAvailableTrucks(startDate, endDate);
                    Console.Clear();
                    Console.WriteLine(
                        FiggleFonts.Slant.Render("Car-Rental")
                    );
                    Console.WriteLine("Please type the id of the Truck you'd like to rent:");
                    foreach (Truck truck in availableTrucks)
                    {
                        Console.WriteLine(truck.Id + "   " + truck.Brand + "   " + truck.Type + "   " + truck.Length + "   " + truck.Length + "   " + truck.Width + "   Price per day = " + truck.PricePerDay);
                    }
                    Console.WriteLine("--------");
                    Console.WriteLine("Id:");
                    int chosenTruckId = Convert.ToInt32(Console.ReadLine());
                    using (Context db = new Context())
                    {
                        try
                        {
                            Truck chosenTruck = db.Trucks.FirstOrDefault(c => c.Id == chosenTruckId);
                            return chosenTruck;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"An Error happened try again please:{e}");
                            throw;
                        }

                    }
                    break;

                case ConsoleKey.D3:
                    List<Motorcycle> availableMotorcycles = vehicleService.GetAvailableMotorcycles(startDate, endDate);
                    Console.Clear();
                    Console.WriteLine(
                        FiggleFonts.Slant.Render("Car-Rental")
                    );
                    Console.WriteLine("Please type the id of the Motorcycle you'd like to rent:");
                    foreach (Motorcycle motorcycle in availableMotorcycles)
                    {
                        Console.WriteLine(motorcycle.Id + "   " + motorcycle.Brand + "   " + motorcycle.Type + "   " + motorcycle.Length + "   " + motorcycle.Length + "   " + motorcycle.Width + "   Price per day = " + motorcycle.PricePerDay);
                    }
                    Console.WriteLine("--------");
                    Console.WriteLine("Id:");
                    int chosenMotorcycleId = Convert.ToInt32(Console.ReadLine());
                    using (Context db = new Context())
                    {
                        try
                        {
                            Motorcycle chosenMotorcycle = db.Motorcycles.FirstOrDefault(c => c.Id == chosenMotorcycleId);
                            return chosenMotorcycle;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"An Error happened try again please:{e}");
                            throw;
                        }

                    }
                default:
                    AskForVehicle(startDate, endDate, true);
                    return null;
                    break;
            }


            //Console.Clear();
            //Console.WriteLine(
            //    FiggleFonts.Slant.Render("Car-Rental")
            //);
            ////---
            //Console.WriteLine("Please type the id of the employee you'd like to work with:");
            //foreach (Vehicle vehicle in availabelVehicles)
            //{
            //    Console.WriteLine(vehicle.Id + "   " + vehicle.Brand + "   " + vehicle.);
            //}
            //Console.WriteLine("--------");
            //Console.WriteLine("Id");
            //int chosenId = Convert.ToInt32(Console.ReadLine());
        }

        public void SeeStatistics(bool isinvalid = false)
        {
            StatisticService statisticService = new StatisticService();
            double revenue = 0;

            Console.Clear();
            Console.WriteLine(
                FiggleFonts.Slant.Render("Statistics")
            );
            Console.WriteLine("What statistics would you like to see?");
            Console.WriteLine("1) Day");
            Console.WriteLine("2) Week");
            Console.WriteLine("3) Month");
            Console.WriteLine("4) Year");
            if (isinvalid)
            {
                Console.WriteLine("Please enter a valid menu option");
            }
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D1:
                    // Day
                    revenue = statisticService.GetRevenue(StatisticsTimespan.Day);
                    Console.WriteLine("Statistics of past day:");
                    break;
                case ConsoleKey.D2:
                    // Week
                    revenue = statisticService.GetRevenue(StatisticsTimespan.Week);
                    Console.WriteLine("Statistics of past week:");
                    break;
                case ConsoleKey.D3:
                    // Month
                    revenue = statisticService.GetRevenue(StatisticsTimespan.Month);
                    Console.WriteLine("Statistics of past month:");
                    break;
                case ConsoleKey.D4:
                    // Year
                    revenue = statisticService.GetRevenue(StatisticsTimespan.Year);
                    Console.WriteLine("Statistics of past year:");
                    break;
                default:
                    SeeStatistics(true);
                    break;
            }

            Console.WriteLine(revenue);
        }
    }
}