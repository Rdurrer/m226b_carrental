﻿using System;
using System.Collections.Generic;
using CarRental.Classes;
using CarRental.Enums;

namespace CarRental.Services.Interfaces
{
    public interface IVehicleService
    {
        //TODO: CHANGE!!!!!!!!!!!!!!!!
        //public List<Vehicle> GetAvailableVehicles(VehicleType type, DateTime startDate, DateTime endDate);


        public List<Car> GetAvailableCars(DateTime startDate, DateTime endDate);
        public List<Truck> GetAvailableTrucks(DateTime startDate, DateTime endDate);
        public List<Motorcycle> GetAvailableMotorcycles(DateTime startDate, DateTime endDate);


        /// <summary>
        /// Cleans vehicle
        /// </summary>
        /// <param name="vehicle">Vehicle to clean</param>
        public void CleanVehice(Vehicle vehicle);

        /// <summary>
        /// Repairs Vehicle
        /// </summary>
        /// <param name="vehicle">Vehicle to repair</param>
        public void RepairVehicle(Vehicle vehicle);

        /// <summary>
        /// Checks the vehicle if it's clean and in a good state
        /// </summary>
        /// <param name="vehicle">Vehicle to check</param>
        public void CheckVehicle(Vehicle vehicle);

        /// <summary>
        /// Checks if the vehicle is available (not locked)
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public bool IsAvailable(Vehicle vehicle);
    }
}