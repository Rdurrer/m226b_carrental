﻿using CarRental.Classes;
using CarRental.Enums;

namespace CarRental.Services.Interfaces
{
    public interface IContractService
    {
        /// <summary>
        /// Adds a contract to DB
        /// </summary>
        /// <param name="contract"></param>
        public void SaveContract(RentalContract contract);

        /// <summary>
        /// Lets the customer pay
        /// </summary>
        /// <param name="paymentMethod">What method the client chose for paying</param>
        /// <param name="contract">The contract thats payed</param>
        /// <param name="paidMoney">The amount of money that is payed</param>
        /// <returns>Reicept of the payment</returns>
        public double Pay(PaymentMethod paymentMethod, RentalContract contract, double paidMoney, double price);

    }
}