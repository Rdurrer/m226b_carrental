﻿using CarRental.Enums;

namespace CarRental.Services.Interfaces
{
    public interface IStatisticService
    {
        /// <summary>
        /// Gets statistic of revenue form specified timespan
        /// </summary>
        /// <param name="statisticsTimespan">Timespan over which the statistics should be calculated</param>
        /// <returns>value of revenue over timespan</returns>
        public double GetRevenue(StatisticsTimespan statisticsTimespan);
    }
}