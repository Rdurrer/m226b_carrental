﻿using System.Collections.Generic;
using CarRental.Classes;

namespace CarRental.Services.Interfaces
{
    public interface IClientService
    {
        /// <summary>
        /// Lists all the contracts a client has and has had
        /// </summary>
        /// <param name="client">The client whose contracts should be listed</param>
        /// <returns>A list of all the contracts the client has</returns>
        public List<RentalContract> RentalContracts(Client client);

        /// <summary>
        /// Gets all the active contracts
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public List<RentalContract> GetActiveContracts(Client client);
        
        /// <summary>
        /// Creates Client and saves it into DB
        /// </summary>
        public Client CreateClient();

        /// <summary>
        /// Returns client with username
        /// </summary>
        /// <param name="username">username of client</param>
        /// <returns></returns>
        public Client GetClient(string username);

        /// <summary>
        /// Unassigns Employee form customer
        /// </summary>
        /// <param name="client"></param>
        /// <param name="employeeId"></param>
        /// <param name="salesEmployee"></param>
        public void UnassignEmployee(Client client, int employeeId = 0, SalesEmployee salesEmployee = null);

        /// <summary>
        /// Assigns Employee to customer
        /// </summary>
        /// <param name="client"></param>
        /// <param name="employeeId"></param>
        /// <param name="salesEmployee"></param>
        public void AssignEmployee(Client client, int employeeId = 0, SalesEmployee salesEmployee = null);
    }
}