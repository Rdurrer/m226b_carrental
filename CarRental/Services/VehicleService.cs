﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarRental.Classes;
using CarRental.Enums;
using CarRental.Services.Interfaces;

namespace CarRental.Services
{
    public class VehicleService : IVehicleService
    {
        //public List<Vehicle> GetAvailableVehicles(VehicleType type, DateTime startDate, DateTime endDate)
        //{
        //    using (Context Db = new Context())
        //    {
        //        return Db.Vehicles.Where(x => x.VehicleType == type && !x.IsLocked && !x.RentalContracts.
        //        Any(v => v.StartTime <= startDate && v.EndTime >= endDate ||
        //        (v.StartTime <= endDate && v.EndTime >= endDate))).ToList();
        //    }
        //}

        public List<Car> GetAvailableCars(DateTime startDate, DateTime endDate)
        {
            using (Context Db = new Context())
            {
                return Db.Cars.Where(x => !x.IsLocked && !x.RentalContracts.
                Any(v => v.StartTime <= startDate && v.EndTime >= endDate ||
                (v.StartTime <= endDate && v.EndTime >= endDate))).ToList();
            }
        }

        public List<Truck> GetAvailableTrucks(DateTime startDate, DateTime endDate)
        {
            using (Context Db = new Context())
            {
                return Db.Trucks.Where(x => !x.IsLocked && !x.RentalContracts.
                Any(v => v.StartTime <= startDate && v.EndTime >= endDate ||
                (v.StartTime <= endDate && v.EndTime >= endDate))).ToList();
            }
        }

        public List<Motorcycle> GetAvailableMotorcycles(DateTime startDate, DateTime endDate)
        {
            using (Context Db = new Context())
            {
                return Db.Motorcycles.Where(x => !x.IsLocked && !x.RentalContracts.
                Any(v => v.StartTime <= startDate && v.EndTime >= endDate ||
                (v.StartTime <= endDate && v.EndTime >= endDate))).ToList();
            }
        }


        public void CheckVehicle(Vehicle vehicle)
        {
            if (vehicle.IsClean && vehicle.IsRepaired)
            {
                vehicle.IsChecked = true;
            }
        }
        public void CleanVehice(Vehicle vehicle)
        {
            vehicle.IsClean = true;
        }
        public void RepairVehicle(Vehicle vehicle)
        {
            vehicle.IsRepaired = true;
        }

        public void LockVehicle(Vehicle vehicle)
        {
            vehicle.IsLocked = true;
        }
        public bool IsAvailable(Vehicle vehicle)
        {
            if (!vehicle.IsLocked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}