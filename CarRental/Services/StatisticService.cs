﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarRental.Classes;
using CarRental.Enums;
using CarRental.Services.Interfaces;

namespace CarRental.Services
{
    public class StatisticService : IStatisticService
    {
        public double GetRevenue(StatisticsTimespan statisticsTimespan)
        {
            double revenue = 0.00;
            DateTime from = DateTime.Today;
            DateTime to = DateTime.Today;
            switch (statisticsTimespan)
            {
                case StatisticsTimespan.Day:
                    from.AddDays(-1);

                    break;
                case StatisticsTimespan.Week:
                    from.AddDays(-7);

                    break;
                case StatisticsTimespan.Month:
                    from.AddDays(-31);

                    break;
                case StatisticsTimespan.Year:
                    from.AddDays(-365);

                    break;
            }

            List<Receipt> receipts = new List<Receipt>();
            using (Context Db = new Context())
            {
                receipts = Db.Receipts.Where(s => s.Date >= from && s.Date <= to).ToList();
            }
            foreach (Receipt receipt in receipts)
            {
                revenue += receipt.Price;
            }
            return revenue;

        }
    }
}