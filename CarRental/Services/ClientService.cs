﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarRental.Classes;
using CarRental.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CarRental.Services
{
    public class ClientService : IClientService
    {
        public List<RentalContract> RentalContracts(Client client)
        {
            List<RentalContract> rentalContracts = new List<RentalContract>();
            using (Context Db = new Context())
            {
                rentalContracts = Db.RentalContracts.Where(s => s.Client == client).ToList();
            }
            return rentalContracts;
        }

        public List<RentalContract> GetActiveContracts(Client client)
        {
            List<RentalContract> rentalContracts = new List<RentalContract>();
            using (Context Db = new Context())
            {
                rentalContracts = Db.RentalContracts.Where(s => s.Client == client && s.IsActive == true).ToList();
            }
            return rentalContracts;
        }

        public Client CreateClient()
        {
            Console.WriteLine("To create a new customer please fill in the credentials:");
            Console.WriteLine("Firstname:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Lastname:");
            string lastName = Console.ReadLine();
            Console.WriteLine("Username: (first letter of firstname + lastname)");
            string username = Console.ReadLine();
            Console.WriteLine("Email address:");
            string emailAddress = Console.ReadLine();
            Console.WriteLine("Street:");
            string street = Console.ReadLine();
            Console.WriteLine("Street-number:");
            bool isValid = false;
            int number = 0;
            do
            {
                try
                {
                    number = Convert.ToInt32(Console.ReadLine());
                    isValid = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Please enter a number!");
                }
            } while (!isValid);

            isValid = false;
            Console.WriteLine("Town:");
            string town = Console.ReadLine();
            Console.WriteLine("Birthdate:");
            DateTime birthday = DateTime.Now;
            do
            {
                try
                {
                    birthday = DateTime.Parse(Console.ReadLine());
                    isValid = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Please enter the date in the following format! dd/mm/yyyy");
                }
            } while (!isValid);
            isValid = false;
            Console.WriteLine("Phone-number");
            string phone = Console.ReadLine();
            Console.WriteLine("Mobile phone-number");
            string phoneMobile = Console.ReadLine();
            Client client = new Client(firstName, lastName, emailAddress, street, number, town, birthday, phone, phoneMobile, username);
            using (Context Db = new Context())
            {
                Db.Clients.Add(client);
                try
                {
                    Db.SaveChanges();
                    return client;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"An Error occured try again please:{e}");
                    throw;
                }
            }
        }

        public void AssignEmployee(Client client, int employeeId = 0, SalesEmployee salesEmployee = null)
        {
            using var db = new Context();
            if (employeeId != 0)
            {
                salesEmployee = db.SalesEmployees.Find(employeeId);
            }

            client.AssignedEmployee = salesEmployee;
            salesEmployee.IsAvailable = false;

            db.SalesEmployees.Update(salesEmployee);
            db.Clients.Update(client);
            db.SaveChanges();
        }

        public void UnassignEmployee(Client client, int employeeId = 0, SalesEmployee salesEmployee = null)
        {
            using var db = new Context();
            if (employeeId != 0)
            {
                salesEmployee = db.SalesEmployees.Find(employeeId);
            }

            client.AssignedEmployee = null;
            salesEmployee.IsAvailable = true;

            db.SalesEmployees.Update(salesEmployee);
            db.Clients.Update(client);
            db.SaveChanges();
        }


        public Client GetClient(string username)
        {
            using (Context db = new Context())
            {
                try
                {
                    Client client = db.Clients.Single(c => c.Username == username);
                    return client;
                }
                catch (Exception e)
                {
                    return null;
                }

            }
        }
    }
}