﻿using CarRental.Classes;
using CarRental.Enums;
using CarRental.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace CarRental.Services
{
    public class ContractService : IContractService
    {
        public void SaveContract(RentalContract contract)
        {
            using (Context Db = new Context())
            {
                Db.RentalContracts.Add(contract);
                Db.SaveChanges();
            }
        }
        public double Pay(PaymentMethod paymentMethod, RentalContract contract, double paidMoney, double price)
        {
            Receipt receipt = new Receipt();
            receipt.Paid = paidMoney;
            double returnMoney = 0;
            double missingMoney;
            if (paymentMethod == PaymentMethod.Card)
            {
                receipt.Paid = paidMoney;
                receipt.Price = paidMoney;
                return returnMoney;
            }
            else
            {
                if (paidMoney >= price)
                {
                    returnMoney = paidMoney - price;
                    return returnMoney;
                }
                else
                {
                    missingMoney = price - paidMoney;
                    Console.WriteLine("That was not enough money! You need to pay " + missingMoney + " more!");
                    return returnMoney;
                }
            }
            
        }



    }
}