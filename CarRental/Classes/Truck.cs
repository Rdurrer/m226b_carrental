﻿using CarRental.Enums;

namespace CarRental.Classes
{
    public class Truck : Vehicle
    {
        public Truck(VehicleType vehicleType, string plate, Brand brand, double weight, double maxWeight, double length,
            double width, Fuel fuel, Tyre tyre, bool isLocked, bool isClean, bool isRepaired, bool isChecked,
            double pricePerDay, TruckType type) : base(vehicleType, plate, brand, weight, maxWeight, length, width,
            fuel, tyre, isLocked, isClean, isRepaired, isChecked, pricePerDay)
        {
            Type = type;
        }

        public Truck()
        {

        }

        public TruckType Type { get; set; }
    }
}