﻿using CarRental.Enums;

namespace CarRental.Classes
{
    public class Motorcycle : Vehicle
    {
        public Motorcycle(VehicleType vehicleType, string plate, Brand brand, double weight, double maxWeight, double length, double width, Fuel fuel, Tyre tyre, bool isLocked, bool isClean, bool isRepaired, bool isChecked, double pricePerDay, MotorcycleType type) : base(vehicleType, plate, brand, weight, maxWeight, length, width, fuel, tyre, isLocked, isClean, isRepaired, isChecked, pricePerDay)
        {
            Type = type;
        }

        public Motorcycle()
        {

        }

        public MotorcycleType Type { get; set; }
    }
}