﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Classes
{
    public abstract class Person
    {
        protected Person()
        {
            
        }

        protected Person(string firstName, string lastName, string emailAddress, string street, int number, string town, DateTime birthday, string phone, string phoneMobile)
        {
            FirstName = firstName;
            LastName = lastName;
            EmailAddress = emailAddress;
            Street = street;
            Number = number;
            Town = town;
            Birthday = birthday;
            Phone = phone;
            PhoneMobile = phoneMobile;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public string Town { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public string PhoneMobile { get; set; }
    }
}