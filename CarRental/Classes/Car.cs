﻿using CarRental.Enums;

namespace CarRental.Classes
{
    public class Car : Vehicle
    {
        public Car(VehicleType vehicleType, string plate, Brand brand, double weight, double maxWeight, double length,
            double width, Fuel fuel, Tyre tyre, bool isLocked, bool isClean, bool isRepaired, bool isChecked,
            double pricePerDay, CarType type) : base(vehicleType, plate, brand, weight, maxWeight, length, width, fuel,
            tyre, isLocked, isClean, isRepaired, isChecked, pricePerDay)
        {
            Type = type;
        }

        public Car()
        {

        }

        public CarType Type { get; set; }
    }
}