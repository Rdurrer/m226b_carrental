﻿using System;

namespace CarRental.Classes
{
    public class MechanicEmployee : Employee
    {
        public MechanicEmployee(string firstName, string lastName, string emailAddress, string street, int number, string town, DateTime birthday, string phone, string phoneMobile, bool isAvailable) : base(firstName, lastName, emailAddress, street, number, town, birthday, phone, phoneMobile, isAvailable)
        {

        }
        public MechanicEmployee()
        {

        }
    }
}