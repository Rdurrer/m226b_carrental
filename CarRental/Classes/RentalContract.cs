﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Classes
{
    public class RentalContract
    {
        public RentalContract(Vehicle vehicle, Client client, SalesEmployee salesemployee,DateTime signDate, DateTime startTime, DateTime endTime, string pickupPoint, string dropOffPoint)
        {
            Vehicle = vehicle;
            Client = client;
            SalesEmployee = salesemployee;
            SignDate = signDate;
            StartTime = startTime;
            EndTime = endTime;
            PickupPoint = pickupPoint;
            DropOffPoint = dropOffPoint;
        }

        public RentalContract()
        {
            
        }
        /// <summary>
        /// Id of the contract
        /// </summary>
        
        public int RentalContractId { get; set; }
        /// <summary>
        /// Vehicle which is rented
        /// </summary>
        public Vehicle Vehicle { get; set; }

        /// <summary>
        /// The client or one person of a group
        /// </summary>
        public Client Client { get; set; }

        /// <summary>
        /// The salesemployee that created the contract
        /// </summary>
        public SalesEmployee SalesEmployee { get; set; }

        /// <summary>
        /// The date the contract was signed
        /// </summary>
        public DateTime SignDate { get; set; }

        /// <summary>
        /// Start time of the contract (Pickup time)
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// End time of the contract (Drop-off time)
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Duration of the rental
        /// </summary>
        public int RentalDuration
        {
            get
            {
                return EndTime.Day - StartTime.Day;
            }
        }

        public bool IsActive { get; set; }

        /// <summary>
        /// The point where the vehicle is picked up
        /// </summary>
        public string PickupPoint { get; set; }

        /// <summary>
        /// The point where the vehicle is dropped off
        /// </summary>
        public string DropOffPoint { get; set; }
    }
}