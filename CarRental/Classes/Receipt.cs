﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Classes
{
    public class Receipt
    {
        public Receipt(double paid, DateTime date)
        {
            Paid = paid;
            Date = date;
        }
        public Receipt()
        {
            
        }
        public int ReceiptId { get; set; }

        /// <summary>
        /// Id of the contract
        /// </summary>
        [Required]
        public int ContractId { get; set; }

        /// <summary>
        /// Contract for which the receipt is
        /// </summary>
        [Required]
        public virtual RentalContract Contract { get; set; }

        /// <summary>
        /// The price of the rental
        /// </summary>
        public double Price
        {
            get
            {
                return Contract.RentalDuration * Contract.Vehicle.PricePerDay;
            }
            set { }
        }

        /// <summary>
        /// The money the client paid
        /// </summary>
        public double Paid { get; set; }

        /// <summary>
        /// Date, the payment took action
        /// </summary>
        public DateTime Date { get; set; }
    }
}