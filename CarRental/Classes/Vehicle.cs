﻿using System.Collections.Generic;
using CarRental.Enums;

namespace CarRental.Classes
{
    public abstract class Vehicle
    {
        protected Vehicle(VehicleType vehicleType, string plate, Brand brand, double weight, double maxWeight, double length,
            double width, Fuel fuel, Tyre tyre, bool isLocked, bool isClean, bool isRepaired, bool isChecked,
            double pricePerDay)
        {
            VehicleType = vehicleType;
            Plate = plate;
            Brand = brand;
            Weight = weight;
            MaxWeight = maxWeight;
            Length = length;
            Width = width;
            Fuel = fuel;
            Tyre = tyre;
            IsLocked = isLocked;
            IsClean = isClean;
            IsRepaired = isRepaired;
            IsChecked = isChecked;
            PricePerDay = pricePerDay;
        }

        protected Vehicle()
        {
        }


        /// <summary>
        /// Id of the vehicle
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Number of seats in the vehicle
        /// </summary>
        /// <summary>
        /// Type of the Vehicle
        /// </summary>
        public VehicleType VehicleType { get; set; }

        /// <summary>
        /// Contracts of Vehicle
        /// </summary>
        public virtual ICollection<RentalContract> RentalContracts { get; set; }

        /// <summary>
        /// The number plate of the vehicle
        /// </summary>
        public string Plate { get; set; }

        /// <summary>
        /// The brand of the vehicle
        /// </summary>
        public Brand Brand { get; set; }

        /// <summary>
        /// Weight of the vehicle in kg
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// Maximum weight of the vehicle in kg
        /// </summary>
        public double MaxWeight { get; set; }

        /// <summary>
        /// Lenght of the vehicle in cm
        /// </summary>
        public double Length { get; set; }

        /// <summary>
        /// Width of the vehicle in cm
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// Type of fuel which the vehicle uses
        /// </summary>
        public Fuel Fuel { get; set; }

        /// <summary>
        /// Type of tyres the vehicle currently has fitted
        /// </summary>
        public Tyre Tyre { get; set; }

        /// <summary>
        /// Locks the vehicle if someone is working on a contract with it
        /// </summary>
        public bool IsLocked { get; set; }

        /// <summary>
        /// If the vehicle is clean or not
        /// </summary>
        public bool IsClean { get; set; }

        /// <summary>
        /// If the Vehicle is repaired or not
        /// </summary>
        public bool IsRepaired { get; set; }

        /// <summary>
        /// If the vehicle was checked or not
        /// </summary>
        public bool IsChecked { get; set; }

        /// <summary>
        /// Price that the vehicle costs per day
        /// </summary>
        public double PricePerDay { get; set; }
    }
}