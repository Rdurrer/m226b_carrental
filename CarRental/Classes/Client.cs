﻿using System;

namespace CarRental.Classes
{
    public class Client : Person
    {
       
        public Client(string firstName, string lastName, string emailAddress, string street, int number, string town, DateTime birthday, string phone, string phoneMobile, string username) : base(firstName, lastName, emailAddress, street, number, town, birthday, phone, phoneMobile)
        {
            Username = username;
        }
        public Client()
        {

        }
        public string Username { get; set; }
        public SalesEmployee AssignedEmployee { get; set; }
    }
}