﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarRental.Migrations
{
    public partial class TestWithKeyBruh : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Receipts_ContractId",
                table: "Receipts");

            migrationBuilder.CreateIndex(
                name: "IX_Receipts_ContractId",
                table: "Receipts",
                column: "ContractId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Receipts_ContractId",
                table: "Receipts");

            migrationBuilder.CreateIndex(
                name: "IX_Receipts_ContractId",
                table: "Receipts",
                column: "ContractId",
                unique: true);
        }
    }
}
