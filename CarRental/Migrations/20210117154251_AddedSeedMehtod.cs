﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CarRental.Migrations
{
    public partial class AddedSeedMehtod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Birthday", "Discriminator", "EmailAddress", "FirstName", "IsAvailable", "LastName", "Number", "Phone", "PhoneMobile", "Street", "Town" },
                values: new object[,]
                {
                    { 1, new DateTime(2000, 3, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "CleaningEmployee", "peter.huerber@carrental.ch", "Peter", true, "Hueber", 3, "341 324 234 23 12", "234 423 243 43 43", "hueberstrasse", "Zug" },
                    { 2, new DateTime(2001, 4, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "CleaningEmployee", "samuel.müller@carrental.ch", "Samuel", true, "Müller", 3, "341 234 234 43 12", "234 234 243 54 43", "müllerstrasse", "Zug" }
                });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Birthday", "Discriminator", "EmailAddress", "FirstName", "LastName", "Number", "Phone", "PhoneMobile", "Street", "Town", "Username" },
                values: new object[,]
                {
                    { 7, new DateTime(2006, 3, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Client", "alessio.seeberger@roche.com", "Alessio", "Seeberger", 8, "076 123 456 43 54", "123 543 321 12 34", "seebergerstrasse", "Zug", "aseeberger" },
                    { 8, new DateTime(2003, 4, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Client", "rafael.durrer@roche.com", "Rafael", "Durer", 7, "076 123 342 43 23", "325 543 423 12 34", "durrerstrasse", "Zug", "rdurrer" }
                });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Birthday", "Discriminator", "EmailAddress", "FirstName", "IsAvailable", "LastName", "Number", "Phone", "PhoneMobile", "Street", "Town" },
                values: new object[,]
                {
                    { 5, new DateTime(1970, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "MechanicEmployee", "louis.england@carrental.ch", "Louis", true, "England", 54, "435 324 675 87 43", "345 546 456 43 54", "englandstrasse", "Baar" },
                    { 6, new DateTime(2000, 7, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "MechanicEmployee", "last.one@carrental.ch", "Last", true, "One", 11, "432 675 234 54 12", "879 987 456 43 23", "onestrasse", "Cham" },
                    { 3, new DateTime(2005, 3, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "SalesEmployee", "dario.buehler@carrental.ch", "Dario", true, "Bühler", 69, "234 324 546 23 12", "657 234 678 54 43", "bühlerstrasse", "Bern" },
                    { 4, new DateTime(2003, 3, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "SalesEmployee", "linus.gattiker@carrental.ch", "Linus", true, "Gattiker", 23, "432 567 433 65 34", "345 657 678 54 43", "gattikerstrasse", "Zürich" }
                });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "Brand", "Discriminator", "Fuel", "IsChecked", "IsClean", "IsLocked", "IsRepaired", "Length", "MaxWeight", "Plate", "PricePerDay", "Car_Type", "Tyre", "VehicleType", "Weight", "Width" },
                values: new object[,]
                {
                    { 1, 8, "Car", 0, false, true, false, true, 2.0, 2000.0, "ZG-300", 20.0, 0, 1, 0, 1500.0, 1.0 },
                    { 2, 9, "Car", 0, false, true, false, true, 3.0, 4000.0, "ZG-400", 40.0, 3, 0, 0, 1600.0, 2.0 }
                });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "Brand", "Discriminator", "Fuel", "IsChecked", "IsClean", "IsLocked", "IsRepaired", "Length", "MaxWeight", "Plate", "PricePerDay", "Motorcycle_Type", "Tyre", "VehicleType", "Weight", "Width" },
                values: new object[,]
                {
                    { 5, 11, "Motorcycle", 1, true, true, false, true, 2.0, 1050.0, "ZG-120", 12.0, 0, 2, 2, 1000.0, 1.0 },
                    { 6, 0, "Motorcycle", 1, true, true, false, true, 2.0, 400.0, "ZG-10", 10.0, 1, 2, 2, 300.0, 1.0 }
                });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "Brand", "Discriminator", "Fuel", "IsChecked", "IsClean", "IsLocked", "IsRepaired", "Length", "MaxWeight", "Plate", "PricePerDay", "Type", "Tyre", "VehicleType", "Weight", "Width" },
                values: new object[,]
                {
                    { 3, 0, "Truck", 2, true, true, false, true, 4.0, 4000.0, "ZG-350", 40.0, 0, 2, 1, 3000.0, 3.0 },
                    { 4, 8, "Truck", 0, true, true, false, true, 3.0, 4000.0, "ZG-420", 35.0, 1, 0, 1, 3000.0, 2.0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "People",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Vehicles",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
