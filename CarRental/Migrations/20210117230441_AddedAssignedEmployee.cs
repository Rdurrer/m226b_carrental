﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarRental.Migrations
{
    public partial class AddedAssignedEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AssignedEmployeeId",
                table: "People",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_People_AssignedEmployeeId",
                table: "People",
                column: "AssignedEmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_People_People_AssignedEmployeeId",
                table: "People",
                column: "AssignedEmployeeId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_People_People_AssignedEmployeeId",
                table: "People");

            migrationBuilder.DropIndex(
                name: "IX_People_AssignedEmployeeId",
                table: "People");

            migrationBuilder.DropColumn(
                name: "AssignedEmployeeId",
                table: "People");
        }
    }
}
