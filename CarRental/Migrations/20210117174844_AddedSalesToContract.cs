﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarRental.Migrations
{
    public partial class AddedSalesToContract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SalesEmployeeId",
                table: "RentalContracts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RentalContracts_SalesEmployeeId",
                table: "RentalContracts",
                column: "SalesEmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_RentalContracts_People_SalesEmployeeId",
                table: "RentalContracts",
                column: "SalesEmployeeId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RentalContracts_People_SalesEmployeeId",
                table: "RentalContracts");

            migrationBuilder.DropIndex(
                name: "IX_RentalContracts_SalesEmployeeId",
                table: "RentalContracts");

            migrationBuilder.DropColumn(
                name: "SalesEmployeeId",
                table: "RentalContracts");
        }
    }
}
