﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using CarRental.Classes;
using CarRental.Enums;

namespace CarRental
{
    public class Context : DbContext
    {
        /// <summary>
        /// Class for DB connection
        /// </summary>
        public DbSet<Person> People { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<CleaningEmployee> CleaningEmployees { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<MechanicEmployee> MechanicEmployees { get; set; }
        public DbSet<Motorcycle> Motorcycles { get; set; }
        public DbSet<Receipt> Receipts { get; set; }
        public DbSet<RentalContract> RentalContracts { get; set; }
        public DbSet<SalesEmployee> SalesEmployees { get; set; }
        public DbSet<Truck> Trucks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=RD-TOWER\SQLEXPRESS;Database=CarRental;Trusted_Connection=True;");
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Creating car dummy data
            Car car1 = new Car(VehicleType.car, "ZG-300", Brand.Audi, 1500, 2000, 2, 1, Fuel.Diesel, Tyre.Dry, false,
                true, true, false, 20.00, CarType.Economy);
            car1.Id = 1;

            Car car2 = new Car(VehicleType.car, "ZG-400", Brand.Dodge, 1600, 4000, 3, 2, Fuel.Diesel, Tyre.Snow, false,
                true, true, false, 40.00, CarType.Sports);
            car2.Id = 2;

            modelBuilder.Entity<Car>().HasData(car1, car2);

            // Creating truck dummy data
            Truck truck1 = new Truck(VehicleType.truck, "ZG-350", Brand.Mercedes, 3000, 4000, 4, 3, Fuel.Kerosin,
                Tyre.Slick, false, true, true, true, 40.00, TruckType.Big);
            truck1.Id = 3;

            Truck truck2 = new Truck(VehicleType.truck, "ZG-420", Brand.Audi, 3000, 4000, 3, 2, Fuel.Diesel,
                Tyre.Snow, false, true, true, true, 35.00, TruckType.Small);
            truck2.Id = 4;

            modelBuilder.Entity<Truck>().HasData(truck1, truck2);

            // Creating motocycle dummy data
            Motorcycle motorcycle1 = new Motorcycle(VehicleType.motorcycle, "ZG-120", Brand.Chrysler, 1000, 1050, 2, 1,
                Fuel.Bleifrei, Tyre.Slick, false, true, true, true, 12.00, MotorcycleType.Scooter);
            motorcycle1.Id = 5;

            Motorcycle motorcycle2 = new Motorcycle(VehicleType.motorcycle, "ZG-10", Brand.Mercedes, 300, 400, 2, 1,
                Fuel.Bleifrei, Tyre.Slick, false, true, true, true, 10.00, MotorcycleType.Motocross);
            motorcycle2.Id = 6;

            modelBuilder.Entity<Motorcycle>().HasData(motorcycle1, motorcycle2);

            // Creating client dummy data
            Client client1 = new Client("Alessio", "Seeberger", "alessio.seeberger@roche.com", "seebergerstrasse", 8,
                "Zug", new DateTime(2006, 3, 12), "076 123 456 43 54", "123 543 321 12 34", "aseeberger");
            client1.Id = 7;

            Client client2 = new Client("Rafael", "Durer", "rafael.durrer@roche.com", "durrerstrasse", 7,
                "Zug", new DateTime(2003, 4, 24), "076 123 342 43 23", "325 543 423 12 34", "rdurrer");
            client2.Id = 8;

            modelBuilder.Entity<Client>().HasData(client1, client2);

            // Creating cleaningEmployee dummy data
            CleaningEmployee cleaningEmployee1 = new CleaningEmployee("Peter", "Hueber", "peter.huerber@carrental.ch",
                "hueberstrasse", 3, "Zug", new DateTime(2000, 3, 12), "341 324 234 23 12", "234 423 243 43 43", true);
            cleaningEmployee1.Id = 1;

            CleaningEmployee cleaningEmployee2 = new CleaningEmployee("Samuel", "Müller", "samuel.müller@carrental.ch",
                "müllerstrasse", 3, "Zug", new DateTime(2001, 4, 12), "341 234 234 43 12", "234 234 243 54 43", true);
            cleaningEmployee2.Id = 2;

            modelBuilder.Entity<CleaningEmployee>().HasData(cleaningEmployee1, cleaningEmployee2);

            //Creating salesemployee dummy data
            SalesEmployee salesEmployee1 = new SalesEmployee("Dario", "Bühler", "dario.buehler@carrental.ch",
                "bühlerstrasse", 69, "Bern", new DateTime(2005, 3, 12), "234 324 546 23 12", "657 234 678 54 43", true);
            salesEmployee1.Id = 3;

            SalesEmployee salesEmployee2 = new SalesEmployee("Linus", "Gattiker", "linus.gattiker@carrental.ch",
                "gattikerstrasse", 23, "Zürich", new DateTime(2003, 3, 23), "432 567 433 65 34", "345 657 678 54 43", true);
            salesEmployee2.Id = 4;

            modelBuilder.Entity<SalesEmployee>().HasData(salesEmployee1, salesEmployee2);

            // Creating mechanicemployee dummy data
            MechanicEmployee mechanicEmployee1 = new MechanicEmployee("Louis", "England", "louis.england@carrental.ch",
                "englandstrasse", 54, "Baar", new DateTime(1970, 3, 27), "435 324 675 87 43", "345 546 456 43 54", true);
            mechanicEmployee1.Id = 5;

            MechanicEmployee mechanicEmployee2 = new MechanicEmployee("Last", "One", "last.one@carrental.ch",
                "onestrasse", 11, "Cham", new DateTime(2000, 7, 23), "432 675 234 54 12", "879 987 456 43 23", true);
            mechanicEmployee2.Id = 6;

            modelBuilder.Entity<MechanicEmployee>().HasData(mechanicEmployee1, mechanicEmployee2);

        }
    }
}